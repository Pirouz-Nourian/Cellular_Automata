import Rhino.Geometry as rg
import Grasshopper as gh
import scriptcontext as sc


def TwentysixNeighbourhood(self):
    BC=[]
    for i in range(0,len(M)):
        BC.Add([])
        n=M[i].Vertices.Count
        VerList=M[i].Vertices
        for j in range (0,n):
            for k in range (0,len(M)):
                if k!=i:
                    if VerList[j] in M[k].Vertices:
                        if k not in BC[i]:
                            BC[i].Add(k)
    return BC

FF=TwentysixNeighbourhood(M)
