Mesh IcosahedralGlobe(Plane BP, double R, out Plane DicePlaneBasis)
  {
    Mesh IcosahedralMesh = Icosahedron(BP, R);
    double phi = 0.5 * (1 + Math.Sqrt(5));
    double theta = Math.Asin(1 / Math.Sqrt(1 + phi * phi));

    //IcosahedralMesh.Transform(xForm);
    //IcosahedralMesh.Rotate(-theta, BP.ZAxis, BP.Origin);
    //IcosahedralMesh.Rotate(0.1 * Math.PI, BP.XAxis, BP.Origin);
    //IcosahedralMesh.Rotate(Math.PI / 3, BP.YAxis, BP.Origin);
    //IcosahedralMesh.Rotate(-0.5 * Math.PI, BP.YAxis, BP.Origin);
    //IcosahedralMesh.Rotate(-0.5 * Math.PI, BP.ZAxis, BP.Origin);

    Point3d v0 = IcosahedralMesh.Vertices[0];
    Point3d v4 = IcosahedralMesh.Vertices[4];
    Point3d v8 = IcosahedralMesh.Vertices[8];
    Vector3d DiceZAxis = new Vector3d(v0 + v4 + v8);//new Vector3d(1, 1, 1);//
    DiceZAxis.Unitize();
    Vector3d DiceYAxis = v4 - v8;
    DiceYAxis.Unitize();
    Vector3d DiceXAxis = Vector3d.CrossProduct(DiceYAxis, DiceZAxis);
    DiceXAxis.Unitize();
    Plane DicePlane = new Plane(BP.Origin, DiceXAxis, DiceYAxis);
    Rhino.Geometry.Transform xForm = Rhino.Geometry.Transform.PlaneToPlane(DicePlane, BP);
    Mesh DiceMesh = IcosahedralMesh.DuplicateMesh();
    DiceMesh.Transform(xForm);

    //IcosahedralMesh.Transform(xForm);
    //double alpha=Math.Atan2(DiceZAxis.Z, DiceZAxis.Length);
    //IcosahedralMesh.Rotate(0.5*Math.PI-alpha, BP.YAxis, BP.Origin);

    //    Vector3d DiceXAxis = Vector3d.CrossProduct(DiceYAxis, DiceZAxis);
    //    Plane DicePlane = new Plane(BP.Origin, DiceXAxis, DiceYAxis);
    //


    DicePlaneBasis = DicePlane;
    return DiceMesh;
  }
  Mesh Icosahedron(Plane BP, double R)
  {
    double phi = 0.5 * (1 + Math.Sqrt(5));

    R = R / (Math.Sqrt(1 + phi * phi));

    /*
    (0, ±φ, ±1)
    (±φ, ±1, 0)
    (±1, 0, ±φ)
    */

    Vector3d e1 = BP.XAxis;
    Vector3d e2 = BP.YAxis;
    Vector3d e3 = BP.ZAxis;

    Plane P12 = new Plane(BP.Origin, e1, e2);
    Plane P23 = new Plane(BP.Origin, e2, e3);
    Plane P31 = new Plane(BP.Origin, e3, e1);

    Point3d V12_NE = P12.PointAt(+R * phi, +R);
    Point3d V12_NW = P12.PointAt(-R * phi, +R);
    Point3d V12_SW = P12.PointAt(-R * phi, -R);
    Point3d V12_SE = P12.PointAt(+R * phi, -R);
    Point3d[] VXY = new Point3d[]{V12_NE,V12_NW,V12_SW,V12_SE};//v0,v1,v2,v3

    Point3d V23_NE = P23.PointAt(+R * phi, +R);
    Point3d V23_NW = P23.PointAt(-R * phi, +R);
    Point3d V23_SW = P23.PointAt(-R * phi, -R);
    Point3d V23_SE = P23.PointAt(+R * phi, -R);
    Point3d[] VYZ = new Point3d[]{V23_NE,V23_NW,V23_SW,V23_SE};//v4,v5,v6,v7

    Point3d V31_NE = P31.PointAt(+R * phi, +R);
    Point3d V31_NW = P31.PointAt(-R * phi, +R);
    Point3d V31_SW = P31.PointAt(-R * phi, -R);
    Point3d V31_SE = P31.PointAt(+R * phi, -R);
    Point3d[] VZX = new Point3d[]{V31_NE,V31_NW,V31_SW,V31_SE};//v8,v9,v10,v11


    Mesh Icosahedron = new Mesh();
    Point3d[] vertices = new Point3d[]{V12_NE,V12_NW,V12_SW,V12_SE,V23_NE,V23_NW,V23_SW,V23_SE,V31_NE,V31_NW,V31_SW,V31_SE};
    Icosahedron.Vertices.AddVertices(vertices);

    Icosahedron.Faces.AddFace(0, 4, 8);
    Icosahedron.Faces.AddFace(1, 11, 4);
    Icosahedron.Faces.AddFace(2, 5, 11);
    Icosahedron.Faces.AddFace(3, 8, 5);

    Icosahedron.Faces.AddFace(0, 9, 7);
    Icosahedron.Faces.AddFace(1, 7, 10);

    Icosahedron.Faces.AddFace(3, 6, 9);

    Icosahedron.Faces.AddFace(0, 7, 4);
    Icosahedron.Faces.AddFace(1, 4, 7);

    Icosahedron.Faces.AddFace(3, 5, 6);

    Icosahedron.Faces.AddFace(4, 11, 8);
    Icosahedron.Faces.AddFace(5, 8, 11);
    Icosahedron.Faces.AddFace(6, 10, 9);
    Icosahedron.Faces.AddFace(7, 9, 10);

    Icosahedron.Faces.AddFace(8, 3, 0);
    Icosahedron.Faces.AddFace(9, 0, 3);
    Icosahedron.Faces.AddFace(10, 2, 1);
    Icosahedron.Faces.AddFace(11, 1, 2);

    Icosahedron.Faces.AddFace(2, 6, 5);
    Icosahedron.Faces.AddFace(2, 10, 6);


    return Icosahedron;
  }